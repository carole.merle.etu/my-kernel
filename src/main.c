#include "ioport.h"
#include "gdt.h"
#include "idt.h"

/*qemu-system-x86_64 -boot d -m 3072 -cdrom mykernel.iso -curses*/

#define MEMORY 2048

typedef void (func_t)(void *);

struct ctx_s {
  void* esp;
  void* ebp;
  func_t *fct; 
  void * arg;
  enum {NOT_STARTED, STARTED, FINISHED} state;
  void * stack;
  struct ctx_s * next;
};

void clear_screen();				/* clear screen */
void putc(char aChar);				/* print a single char on screen */
void puts(char *aString);			/* print a string on the screen */
void puthex(int aNumber);			/* print an Hex number on screen */
void yield();
int init_ctx(struct ctx_s *ctx, int stack_size, func_t f, void *args);
int create_ctx(int stack_size, func_t f, void *args);
void switch_to(struct ctx_s * ctx);
char keyboard_map(unsigned char c);
void keyboard();
void compteur();
void * malloc(unsigned char size);

unsigned char memory[MEMORY];
int indice_memory = 0;
struct ctx_s * ctx_courant = 0;
struct ctx_s * ctx_head = 0;
/* print a char on the screen */
int cursor_x=0;					/* here is the cursor position on X [0..79] */
int cursor_y=0;					/* here is the cursor position on Y [0..24] */

/*void f_ping() {
    while(1) {
        puts("A");
    }
}

void f_pong() {
    while(1) {
        puts("2");
    }
}*/

void empty_irq(int_regs_t *r) {}


/* multiboot entry-point with datastructure as arg. */
void main(unsigned int * mboot_info) {
	/* clear the screen */
	clear_screen();
	puts("Early boot.\n"); 
	puts("\t-> Setting up the GDT... ");
	gdt_init_default();
	puts("done\n");

	puts("\t-> Setting up the IDT... ");
	setup_idt();
	puts("OK\n");
	
	puts("\n\n");
 
	/*idt_setup_handler(0, empty_irq);*/
	/* interruption de niveau 1 = interruption d’horloge, par défaut se produit toutes les 18 milli-seconde. 
	on peut associer l’exécution d’une fonction à l’occurence de cette interruption en utilisant idt_setup_handler(,)*/ 
	idt_setup_handler(0, yield);
	idt_setup_handler(1, keyboard);
	 
	__asm volatile("sti");

	/* minimal setup done ! */

/* Question 1.1 : mon premier premier hello world. */
	puts("Hello World");
	puts("\n\n\n");
	
/* Question 1.2 : interroger le controleur clavier. */
  /*  while(1) {
	  if(_inb(0x64) == 0x1D) { //status register
		char c = keyboard_map(_inb(0x60));
			if(c != 0) {
				putc(c);
			}
		}
	}*/

/*Question 2.2 : Un premier démonstrateur.*/
	create_ctx(16384, keyboard, 0);
	create_ctx(16384, compteur, 0);

	for(;;); 
}


void * malloc(unsigned char size) {
	if(indice_memory < MEMORY) {
		void * adr = &memory[indice_memory];
		indice_memory= indice_memory + size + 1;
		return adr;
	}

	return 0;
}


void yield() {
	if(ctx_courant->next == 0) { 
		switch_to(ctx_head);
	} else {
		switch_to(ctx_courant->next); 
	}
}

int init_ctx(struct ctx_s *ctx, int stack_size,  func_t f, void *args) {
	ctx->stack = malloc(stack_size);

	if(ctx->stack == 0) {
        puts("Fatal: failed to allocate memory for ctx_s->stack\n");
        ctx->state = FINISHED;
        return -1;
    } 
  
  	ctx->esp = ctx->stack + stack_size - sizeof(void*); 
 	ctx->ebp = ctx->stack + stack_size - sizeof(void*); 

  	ctx->arg = args;
  	ctx->fct = f;
  
  	ctx->state = NOT_STARTED;

  	return 0;
}

int create_ctx(int stack_size, func_t f, void *args) {
    struct ctx_s* new_ctx;
	new_ctx = (struct ctx_s*)malloc(sizeof(struct ctx_s));
  	
    if(new_ctx != 0) {
		init_ctx(new_ctx, stack_size, f, args);
		if(ctx_head == 0) {
		  	ctx_head = new_ctx;
		  	ctx_courant = ctx_head;
		} else {
		  	struct ctx_s * ctx_last = ctx_head;
		  	while(ctx_last->next != 0) {
				ctx_last = ctx_last->next;
		    }
		  	ctx_last->next = new_ctx;
		}
  	}  
  return 0;
}


void switch_to(struct ctx_s * ctx) {
	if(ctx != 0) {
	  	__asm volatile("cli"); /* cli disables interruptions */

		if(ctx_courant != 0) {
			/* on sauvegarde les pointeurs de pile dans le contexte courant */
			asm("mov %%esp, %0"
			: "=r" (ctx_courant->esp));

			asm("mov %%ebp, %0"
			: "=r" (ctx_courant->ebp));
			  

			/* définit le contexte dont l’adresse est passée en paramètre comme nouveau contexte courant */
			ctx_courant = ctx;

			/* on restaure les registres de pile du contexte passé en paramètre */
		    asm("mov %0, %%esp"
			:
			: "r" (ctx_courant->esp));

			asm("mov %0, %%ebp"
			:
			: "r" (ctx_courant->ebp));

			if(ctx_courant->state == NOT_STARTED) {
				ctx_courant->state = STARTED;
				_outb(0x20, 0x20);  /*pour préciser quelles interruptions on veut ou pas equivalent _mask dans tp ordo */
				__asm volatile("sti"); /* enables interruptions*/
				ctx_courant->fct(ctx_courant->arg);
				ctx_courant->state = FINISHED;
				yield();
			}
				
			if(ctx_courant->state == STARTED) {
				_outb(0x20, 0x20);
				__asm volatile("sti");
				/* on retourne dans le contexte d’exécution passé en paramètre. */
				return;
			}
		}
	}
}

void keyboard() {
	if(_inb(0x64) == 0x1D) { /*status register*/
		char c = keyboard_map(_inb(0x60));
		if(c != 0) {
			putc(c);
		}
  	}
}

void compteur() {
	int i=1;
	while(1) {
		__asm volatile("cli");
		int save_x = cursor_x, save_y = cursor_y;
		cursor_x = 70; 
		cursor_y = 0;
		puthex(i);
		if(i < 2147483647) {
			i++;
		} else {
			i = 1;
		}
		cursor_x = save_x; 
		cursor_y = save_y;
		_outb(0x20, 0x20);
		__asm volatile("sti");
	}
}


/* retourne le code ascii associé à une touche clavier lorsque cela a un sens, ou zero sinon. */
char keyboard_map(unsigned char c) { 
  switch(c){
	case 0x1C:
		return '\n';
	case 0x39:
		return ' ';
	case 0xB:
		return '0';
	case 0x02:
		return '1';
	case 0x03:
		return '2';
	case 0x04:
		return '3';
	case 0x05:
		return '4';
	case 0x06:
		return '5';
	case 0x07:
		return '6';
	case 0x08:
		return '7';
	case 0x09:
		return '8';
	case 0x0A:
		return '9';
	case 0x0C:
		return '-';
	/*case 0x:
	  return '_'; 
	case 0x:
		return '+';*/
	case 0x0D:
		return '=';
	case 0x10:
		return 'q';
	case 0x11:
		return 'w';
	case 0x12:
	  return 'e';
	case 0x13:
		return 'r';
	case 0x14:
		return 't';
	case 0x15:
		return 'y';
	case 0x16:
		return 'u';
	case 0x17:
		return 'i';
	case 0x18:
		return 'o';
	case 0x19:
		return 'p';
	case 0x1E:
		return 'a';
	case 0x1F:
		return 's';
	case 0x20:
		return 'd';
	case 0x21:
		return 'f';
	case 0x22:
		return 'g';
	case 0x23:
		return 'h';
	case 0x24:
		return 'j';
	case 0x25:
		return 'k';
	case 0x26:
		return 'l';
	case 0x27:
		return ';';
	/*case 0x28:
	  return ':';*/
	case 0x28:
		return '\'';
	/*case 0x07:
		return '"';*/
	case 0x2B:
		return '\\';
	/*case 0x09:
	  return '|';*/
	case 0x29:
		return '`';
	case 0x2C:
		return 'z';
	case 0x2D:
		return 'x';
	case 0x2E:
		return 'c';
	case 0x2F:
		return 'v';
	case 0x30:
		return 'b';
	case 0x31:
		return 'n';
	case 0x32:
		return 'm';
	case 0x33:
		return ',';
	/*case 0xB:
		return '<';*/
	case 0x34:
		return '.';
	/*case 0x03:
	  return '>';*/
	case 0x35:
		return '/';
	/*case 0x05:
	  return '?';*/
	case 0x0F:
		return '\t';
	case 0x0E:
		return 0x8;
	case 0x1A:
		return '[';
	case 0x1B:
		return ']';
	/*case 0x04:
	  return '{';
	case 0x05:
	  return '}';*/
	/*case 0x06:
	  return '!';
	case :
	  return '@';
	case 0x03:
	  return '#';
	case 0x04:
	  return '$';
	case 0x05:
	  return '%';
	case 0x06:
	  return '&';
	case :
	  return '*';
	case :
	  return '(';
	case :
	  return ')';
	case :
	  return 'Q';
	case :
	  return 'W';
	case :
	  return 'E';
	case :
	  return 'R';
	case :
	  return 'T';
	case :
	  return 'Y';
	case :
	  return 'U';
	case :
	  return 'I';
	case :
	  return 'O';
	case :
	  return 'P';
	case :
	  return 'A';
	case :
	  return 'S';
	case :
	  return 'D';
	case :
	  return 'F';
	case :
	  return 'G';
	case :
	  return 'H';
	case :
	  return 'J';
	case :
	  return 'K';
	case :
	  return 'L';
	case :
	  return 'Z';
	case :
	  return 'X';
	case :
	  return 'C';
	case :
	  return 'V';
	case :
	  return 'B';
	case :
	  return 'N';
	case :
	  return 'M';*/
  default:
	  return 0;
  }
}


/* base address for the video output assume to be set as character oriented by the multiboot */
unsigned char *video_memory = (unsigned char *) 0xB8000;

/* clear screen */
void clear_screen() {
  int i;
  for(i=0;i<80*25;i++) { 			/* for each one of the 80 char by 25 lines */
	video_memory[i*2+1]=0x0F;			/* color is set to black background and white char */
	video_memory[i*2]=(unsigned char)' '; 	/* character shown is the space char */
  }
}

/* print a string on the screen */
void puts(char *aString) {
  char *current_char=aString;
  while(*current_char!=0) {
	putc(*current_char++);
  }
}

/* print an number in hexa */
char *hex_digit="0123456789ABCDEF";
void puthex(int aNumber) {
  int i;
  int started=0;
  for(i=28;i>=0;i-=4) {
	int k=(aNumber>>i)&0xF;
	if(k!=0 || started) {
	  putc(hex_digit[k]);
	  started=1;
	}
  }
}

void setCursor() {
  int cursor_offset = cursor_x+cursor_y*80;
  _outb(0x3d4,14);
  _outb(0x3d5,((cursor_offset>>8)&0xFF));
  _outb(0x3d4,15);
  _outb(0x3d5,(cursor_offset&0xFF));
}

void putc(char c) {
  if(cursor_x>79) {
	cursor_x=0;
	cursor_y++;
  }
  if(cursor_y>24) {
	cursor_y=0;
	clear_screen();
  }
  switch(c) {					/* deal with a special char */
	case '\r': cursor_x=0; break;		/* carriage return */
	case '\n': cursor_x=0; cursor_y++; break; 	/* new ligne */	
	case 0x8 : if(cursor_x>0) cursor_x--; break;/* backspace */
	case 0x9 : cursor_x=(cursor_x+8)&~7; break;	/* tabulation */
						/* or print a simple character */
	default  : 
	  video_memory[(cursor_x+80*cursor_y)*2]=c;
	  cursor_x++;
	  break;
  }
  setCursor();
}


